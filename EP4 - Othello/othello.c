/****************************************************************
    Nome: Thiago Guerrero Balera
    NUSP: 11275297

    Fonte e comentários: 
    Feitos por mim mesmo.
****************************************************************/
#include <stdio.h>

int checagem (int tabuleiro[8][8], int cor, int l, int c, int tipo){
    int i, j, check;

    /*Faz uma checagem de acordo com o tipo recebido buscando varrer todos os elementos
     da cor oposta na direção escolhida e verificando se a sequência termina com um elemento da
     mesma cor da inicial. Se encontrou, check vira 1, caso contrário, check recebe 0.*/

    /*Tipo de checagem:
    0 - Checagem na vertical para cima.
    1 - Checagem na vertical para baixo.
    2 - Checagem na horizontal para esquerda.
    3 - Checagem na horizontal para direita.
    4 - Checagem na diagonal direita para cima.
    5 - Checagem na diagonal direita para baixo.
    6 - Checagem na diagonal esquerda para cima.
    7 - Checagem na diagonal esquerda para baixo.
    */

    if (tipo == 0){
        for (i = l-1, check=0; i >= 0 && tabuleiro[i][c] == -cor; i--){}
        if (i >=0 && tabuleiro[i][c] == cor)
            check = 1;
    }

    else if (tipo == 1){
        for (i = l+1, check=0; i < 8 && tabuleiro[i][c] == -cor; i++){}
        if (i < 8 && tabuleiro[i][c] == cor)
            check = 1;
    }

    else if (tipo == 2){
        for (j = c-1, check=0; j >= 0 && tabuleiro[l][j] == -cor; j--){}
        if (j >=0 && tabuleiro[l][j] == cor)
            check = 1;
    }

    else if (tipo == 3){
        for (j = c+1, check=0; j < 8 && tabuleiro[l][j] == -cor; j++){}
        if (j < 8 && tabuleiro[l][j] == cor)
            check = 1;
    }

    else if (tipo == 4){
        for (i = l-1, j = c+1, check=0; i >= 0 && j < 8 && tabuleiro[i][j] == -cor; i--, j++){}
        if (i >= 0 && j < 8 && tabuleiro[i][j] == cor)
            check = 1;
    }

    else if (tipo == 5){
        for (i = l+1, j = c+1, check=0; i < 8 && j < 8 && tabuleiro[i][j] == -cor; i++, j++){}
        if (i < 8 && j < 8 && tabuleiro[i][j] == cor)
            check = 1;
    }

    else if (tipo == 6){
        for (i = l-1, j = c-1, check=0; i >= 0 && j >= 0 && tabuleiro[i][j] == -cor; i--, j--){}
        if (i >=0 && j >= 0 && tabuleiro[i][j] == cor)
            check = 1;
    }

    else if (tipo == 7){
        for (i = l+1, j = c-1, check=0; i < 8 && j >= 0 && tabuleiro[i][j] == -cor; i++, j--){}
        if (i < 8 && j >= 0 && tabuleiro[i][j] == cor)
            check = 1;
    }
    return (check);
}

int podejogar (int tabuleiro[8][8], int cor, int l, int c){
    int check=0, i, j;

    if (tabuleiro[l][c] != 0)
        return 0;

    if (l-1 < 0)
        i = l;
    else
        i = l-1;

    /*Faz a checagem nas 8 casas ao redor da jogada buscando um elemento da cor oposta para
     começar a segunda checagem, que será feita através da função "checagem".*/
    while (i <= l+1 && i < 8){
        if (c-1 < 0)
            j = c;
        else
            j = c-1;
        while (j <= c+1 && j < 8 && check == 0){
            if (tabuleiro[i][j] == (-cor)){
                if (i == l-1){
                    if (j == c-1)
                        check = checagem(tabuleiro, cor, i, j, 6);
                    else if (j == c)
                        check = checagem(tabuleiro, cor, i, j, 0);
                    else if( j == c+1)
                        check = checagem(tabuleiro, cor, i, j, 4);
                }

                else if (i == l){
                    if (j == c-1)
                        check = checagem(tabuleiro, cor, i, j, 2);
                    else if( j == c+1)
                        check = checagem(tabuleiro, cor, i, j, 3);
                }

                else if (i == l+1){
                    if (j == c-1)
                        check = checagem(tabuleiro, cor, i, j, 7);
                    else if (j == c)
                        check = checagem(tabuleiro, cor, i, j, 1);
                    else if ( j == c+1)
                        check = checagem(tabuleiro, cor, i, j, 5);
                }
            }
            j++;
        }
        i++;
    }
    return (check);
}

void joga (int tabuleiro[8][8], int cor, int l, int c){
    int i, j, count, check;

    tabuleiro[l][c]= cor;

    /*Verifica através da função "checagem" se existe elementos a serem substituidos nas 8 direções
    possíveis (para cima, para baixo, diagonal esquerda para cima, etc), se sim, faz as alterações
    no tabuleiro*/

    for (count=0, check=0; count < 9; count++){
        check = checagem(tabuleiro, cor, l, c, count);
        if (check == 1 && count == 0){
            for (i=l-1; tabuleiro[i][c] != cor; i--)
                if (tabuleiro[i][c] == -cor)
                    tabuleiro [i][c] = cor;
        }
        else if (check == 1 && count == 1){
            for (i=l+1; tabuleiro[i][c] != cor; i++)
                if (tabuleiro[i][c] == -cor)
                    tabuleiro [i][c] = cor;
        }
        else if (check == 1 && count == 2){
            for (i=c-1; tabuleiro[l][i] != cor; i--)
                if (tabuleiro[l][i] == -cor)
                    tabuleiro [l][i] = cor;
        }
        else if (check == 1 && count == 3){
            for (i=c+1; tabuleiro[l][i] != cor; i++)
                if (tabuleiro[l][i] == -cor)
                    tabuleiro [l][i] = cor;
        }
        else if (check == 1 && count == 4){
            for (i=l-1, j=c+1; tabuleiro[i][j] != cor; i--, j++)
                if (tabuleiro[i][j] == -cor)
                    tabuleiro [i][j] = cor;
        }
        else if (check == 1 && count == 5){
            for (i=l+1, j=c+1; tabuleiro[i][j] != cor; i++, j++)
                if (tabuleiro[i][j] == -cor)
                    tabuleiro [i][j] = cor;
        }
        else if (check == 1 && count == 6){
            for (i=l-1, j=c-1; tabuleiro[i][j] != cor; i--, j--)
                if (tabuleiro[i][j] == -cor)
                    tabuleiro [i][j] = cor;
        }
        else if (check == 1 && count == 7){
            for (i=l+1, j=c-1; tabuleiro[i][j] != cor; i++, j--)
                if (tabuleiro[i][j] == -cor)
                    tabuleiro [i][j] = cor;
        }
    }
}

void escolhejogada (int tabuleiro[8][8], int cor, int *linha, int *coluna){
    /*

                              BACANA         BACANA

         
    */
    int jogou=0, i, j;

    /*Nessa primeira passagem, o bot tenta jogar em um dos 4 cantos do tabuleiro:*/

    if (podejogar(tabuleiro, cor, 0, 0) == 1){
        *linha = 0;
        *coluna = 0;
        jogou = 1;
    }
        
    else if (podejogar(tabuleiro, cor, 0, 7) == 1){
        *linha = 0;
        *coluna = 7;
        jogou = 1;
    }

    else if (podejogar(tabuleiro, cor, 7, 0) == 1){
        *linha = 7;
        *coluna = 0;
        jogou = 1;
    }
    
    else if (podejogar(tabuleiro, cor, 7, 7) == 1){
        *linha = 7;
        *coluna = 7;
        jogou = 1;
    }
    //####################################################################################

    /*Nessa segunda passagem, caso o bot tenha algum dos cantos, ele tenta consolidar a sua dominância
     sobre o canto do tabuleiro verificando se as posições ao redor desse canto estão seguras:*/

    if (tabuleiro[0][0] == cor && jogou == 0){ //Primeiro canto.
        if (tabuleiro[0][1] == -cor){
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, 0, i) == 1){
                    *linha = 0;
                    *coluna = i;
                    jogou = 1;
                }
        }
        if (tabuleiro[1][0] == -cor){
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, i, 0) == 1){
                    *linha = i;
                    *coluna = 0;
                    jogou = 1;
                }
        }
        if (tabuleiro[1][1] == -cor){
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, i, 1) == 1){
                    *linha = i;
                    *coluna = 1;
                    jogou = 1;
                }
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, 1, i) == 1){
                    *linha = 1;
                    *coluna = i;
                    jogou = 1;
                }
            for (i=2, j=2; i < 8 && jogou == 0; i++, j++)
                if (podejogar(tabuleiro, cor, i, j) == 1){
                    *linha = i;
                    *coluna = j;
                    jogou = 1;
                }
        }
    }

    if (tabuleiro[0][7] == cor && jogou == 0){ //Segundo canto.
        if (tabuleiro[0][6] == -cor){
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, 0, i) == 1){
                    *linha = 0;
                    *coluna = i;
                    jogou = 1;
                }
        }
        if (tabuleiro[1][7] == -cor){
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, i, 7) == 1){
                    *linha = i;
                    *coluna = 7;
                    jogou = 1;
                }
        }
        if (tabuleiro[1][6] == -cor){
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, i, 6) == 1){
                    *linha = i;
                    *coluna = 6;
                    jogou = 1;
                }
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, 1, i) == 1){
                    *linha = 1;
                    *coluna = i;
                    jogou = 1;
                }
            for (i=2, j=5; i < 8 && jogou == 0; i++, j--)
                if (podejogar(tabuleiro, cor, i, j) == 1){
                    *linha = i;
                    *coluna = j;
                    jogou = 1;
                }
        }
    }

    if (tabuleiro[7][0] == cor && jogou == 0){ //Terceiro canto.
        if (tabuleiro[7][1] == -cor){
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, 7, i) == 1){
                    *linha = 7;
                    *coluna = i;
                    jogou = 1;
                }
        }
        if (tabuleiro[6][0] == -cor){
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, i, 0) == 1){
                    *linha = i;
                    *coluna = 0;
                    jogou = 1;
                }
        }
        if (tabuleiro[6][1] == -cor){
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, i, 1) == 1){
                    *linha = i;
                    *coluna = 1;
                    jogou = 1;
                }
            for (i=2; i < 8 && jogou == 0; i++)
                if (podejogar(tabuleiro, cor, 6, i) == 1){
                    *linha = 6;
                    *coluna = i;
                    jogou = 1;
                }
            for (i=5, j=2; i >= 0 && jogou == 0; i--, j++)
                if (podejogar(tabuleiro, cor, i, j) == 1){
                    *linha = i;
                    *coluna = j;
                    jogou = 1;
                }
        }
    }

    if (tabuleiro[7][7] == cor && jogou == 0){ //Quarto canto.
        if (tabuleiro[7][6] == -cor){
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, 7, i) == 1){
                    *linha = 7;
                    *coluna = i;
                    jogou = 1;
                }
        }
        if (tabuleiro[6][7] == -cor){
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, i, 7) == 1){
                    *linha = i;
                    *coluna = 7;
                    jogou = 1;
                }
        }
        if (tabuleiro[6][6] == -cor){
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, i, 6) == 1){
                    *linha = i;
                    *coluna = 6;
                    jogou = 1;
                }
            for (i=5; i >= 0 && jogou == 0; i--)
                if (podejogar(tabuleiro, cor, 6, i) == 1){
                    *linha = 6;
                    *coluna = i;
                    jogou = 1;
                }
            for (i=5, j=5; i >= 0 && jogou == 0; i--, j--)
                if (podejogar(tabuleiro, cor, i, j) == 1){
                    *linha = i;
                    *coluna = j;
                    jogou = 1;
                }
        }
    }
    //####################################################################################

    /*Nessa terceira passagem, o bot busca centralizar o máximo possível as suas jogadas através
    de quadrados delimitados por 3 "for's" diferentes:*/

    for (i=2; i < 6 && jogou == 0; i++) //Quadrado 4x4.
        for (j=2; j < 6 && jogou == 0; j++)
            if (tabuleiro[i][j] == 0 && podejogar(tabuleiro, cor, i, j) == 1){
                *linha = i;
                *coluna = j;
                jogou = 1;
            }

    for (i=1; i < 7 && jogou == 0; i++) //Quadrado 6x6.
        for(j=1; j < 7 && jogou == 0; j++)
            if (tabuleiro[i][j] == 0 && podejogar(tabuleiro, cor, i, j) == 1){
                *linha = i;
                *coluna = j;
                jogou = 1;
            }
        
    for (i=0; i < 8 && jogou == 0; i++) //Quadrado 8x8.
        for(j=0; j < 8 && jogou == 0; j++)
            if (tabuleiro[i][j] == 0 && podejogar(tabuleiro, cor, i, j) == 1){
                *linha = i;
                *coluna = j;
                jogou = 1;
            }
}

void printa (int tabuleiro[8][8]){
    int i,j;
    printf ("\n");
    printf ("\\   ");
    for (i=0; i < 8; i++)
        printf ("  %2d   ", i);
    printf ("\n");
    printf ("    ------------------------------------------------------\n");


    for (i=0; i < 8; i++){
        printf ("%d  |", i);
        for (j=0; j < 8; j++){
            if (tabuleiro[i][j] == -1)
                printf  ("   *  ");
            else if (tabuleiro[i][j] == 1)
                printf  ("   O  ");
            else
                printf  ("      ");
            if (j == 7)
                printf ("\n");
            else
                printf ("|");
        }
        printf ("    ------------------------------------------------------\n");
    }
}

int verificador (int tabuleiro[8][8], int cor){
    int i, j, check=0;

    /*A função checa todos os espaços vazios do tabuleiro e devolve 1 se existe uma jogada 
    possível para a cor e 0 caso contrário.*/

    for (i=0; i < 8 && check == 0; i++)
        for (j=0; j < 8 && check == 0; j++)
            if (tabuleiro[i][j] == 0)
                check= podejogar(tabuleiro, cor, i, j);

    return (check);
}

int main (){
    int tabuleiro[8][8], i, j, linha, coluna, cor, check, count, vez, aux, continua, scoreU, scoreC;
    /*                    LEGENDA:
    check    = Verifica se o usuário ou o computador perderam por uma jogada inválida.
    count    = Contador do jogo.
    Vez      = Alternador de turnos de jogada.
    continua = Verificador de continuidade do jogo.
    scoreU   = Pontuação do usuário.
    scoreC   = Pontuação do computador.
    */

    for (i=0; i < 8; i++)
        for (j=0; j < 8; j++)
            tabuleiro[i][j] = 0;

    tabuleiro[3][3]= 1;
    tabuleiro[3][4]= -1;
    tabuleiro[4][3]= -1;
    tabuleiro[4][4]= 1;

    printf ("\n**********Bem Vindo ao Othello**********\n");
    printf ("\nCom quais peças você deseja começar? (1 : Brancas | -1 : Pretas): ");
    scanf ("%d", &aux);
    cor = -aux;
    printa(tabuleiro);

    if (cor == -1)
        vez=0;
    else
        vez=1;

    for (count=0, continua=1, check=0; count <= 64 && continua == 1; count++, vez++){
        if (verificador(tabuleiro, cor) == 0 && verificador(tabuleiro, -cor) == 0){
            continua=0;
            printf ("\nSem jogadas válidas para nenhum dos lados. O jogo acabou!\n");
        }

        if (vez % 2 == 0 && continua == 1){
            printf ("\n##################################################\n");
            printf ("                  Vez do computador                  ");
            printf ("\n##################################################\n");
            if (verificador(tabuleiro, cor) == 1){
                escolhejogada(tabuleiro, cor, &linha, &coluna);
                if (podejogar(tabuleiro, cor, linha, coluna) == 1)
                    joga(tabuleiro, cor, linha, coluna);

                else{
                    printf ("\nO computador fez uma jogada inválida! Você ganhou!\n");
                    continua=0;
                    check =1;
                }
            }
            else
                printf ("\nO computador está sem jogadas válidas possíveis! Jogue novamente!\n");
        }

        else if (continua == 1){
            printf ("\n##################################################\n");
            printf ("                     Sua vez                     ");
            printf ("\n##################################################\n");
            if (verificador(tabuleiro, -cor) == 1){
                printf ("Digite a linha: ");
                scanf ("%d", &linha);
                printf ("Digite a coluna: ");
                scanf ("%d", &coluna);
                if (podejogar(tabuleiro, -cor, linha, coluna) == 1)
                    joga (tabuleiro, -cor, linha, coluna);
                else{
                    printf ("\nVocê fez uma jogada inválida! O computador ganhou!\n");
                    continua=0;
                    check=1;
                }
            }
            else
                printf ("\nVocê está sem jogadas válidas possíveis!\n");
        }
        printa(tabuleiro);
    }
    
    if (check == 0){
        for (i=0, scoreU=0, scoreC=0; i < 8; i++)
            for (j=0; j < 8; j++){
                if (cor == 1){
                    if (tabuleiro[i][j] == 1)
                        scoreC++;
                    else
                        scoreU++;
                }
                else if (cor == -1){
                    if (tabuleiro[i][j] == 1)
                        scoreU++;
                    else
                        scoreC++;
                }
            }
        printf ("\n##################################################\n");
        printf ("                   RESULTADO                   ");
        printf ("\n##################################################\n");
        if (scoreU > scoreC)
            printf ("\nVocê ganhou! Placar: %2d x %2d", scoreU, scoreC);
        
        else if (scoreU == scoreC)
            printf ("\nEmpatou! Placar: %2d x %2d", scoreU, scoreC);

        else
            printf ("\nO computador ganhou! Placar: %2d x %2d", scoreC, scoreU);
    }

    return 0;
}