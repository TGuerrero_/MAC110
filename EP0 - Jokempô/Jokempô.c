/*
  Thiago Guerrero
  NºUSP: 11275297
  MAC 110 - 2019
 */

#include <stdio.h>

int main() {
  int player1, player2, restart;
  restart = 0;

  while (restart == 0){
    printf ("\n***Bem vindo ao Jokempô***\n");
    printf ("\nDigite o que o primeiro jogador jogou (0 - Pedra, 1 - Tesoura e 2 - Papel): ");
    scanf ("%d", &player1);
    printf ("Digite o que o segundo jogador jogou (0 - Pedra, 1 - Tesoura e 2 - Papel): ");
    scanf ("%d", &player2);

    if (player1 == player2)
      printf("\nEmpatou!\n");

    if (player1 == 0){
      if (player2 == 1)
        printf ("\nO primeiro jogador ganhou!\n");
      if (player2 == 2)
        printf ("\nO segundo jogador ganhou!\n");
    }

    if (player1 == 1){
      if (player2 == 0)
        printf ("\nO segundo jogador ganhou!\n");
      if (player2 == 2)
        printf ("\nO primeiro jogador ganhou!\n");
    }

    if (player1 == 2){
      if (player2 == 0)
        printf ("\nO primeiro jogador ganhou!\n");
      if (player2 ==1)
        printf ("\nO segundo jogador ganhou!\n");
    }
    printf ("\nDeseja jogar novamente? (0 - Sim, 1 - Não): ");
    scanf ("%d", &restart);
    }

  while (restart == 1)
    return 0;
  }