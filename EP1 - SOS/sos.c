/****************************************************************
    Nome: Thiago Guerrero Balera
    NUSP: 11275297

    Fonte e comentários: 
    Feitos por mim mesmo.
****************************************************************/
#include <stdio.h>

int jogadaplayer (int jogada, int linha, int coluna, int tabuleiro){

    if (linha == 1 && coluna == 1)
        tabuleiro = (tabuleiro + jogada);

    else if (linha == 1 && coluna == 2)
        tabuleiro = (tabuleiro + jogada*3);

    else if (linha == 1 && coluna == 3)
        tabuleiro = (tabuleiro + jogada*3*3);

    else if (linha == 2 && coluna == 1)
        tabuleiro = (tabuleiro + jogada*3*3*3);

    else if (linha == 2 && coluna == 2)
        tabuleiro = (tabuleiro + jogada*3*3*3*3);

    else if (linha == 2 && coluna == 3)
        tabuleiro = (tabuleiro + jogada*3*3*3*3*3);

    else if (linha == 3 && coluna == 1)
        tabuleiro = (tabuleiro + jogada*3*3*3*3*3*3);

    else if (linha == 3 && coluna == 2)
        tabuleiro = (tabuleiro + jogada*3*3*3*3*3*3*3);

    else if (linha == 3 && coluna == 3)
        tabuleiro = (tabuleiro + jogada*3*3*3*3*3*3*3*3);
    return (tabuleiro);
}

int jogadabot (int tabuleiro){
    int cpu=0, tabuleiroprint, i;

    for (i=1, tabuleiroprint = tabuleiro; cpu == 0; i = i * 3){
        if (tabuleiroprint % 3 == 0){
            tabuleiro = (tabuleiro + 1*i);
            cpu++;
            printf  ("\nTabuleiro após minha jogada:\n");
            }
        tabuleiroprint = tabuleiroprint / 3;
    }
    return (tabuleiro);
}

int maintabuleiro (int tabuleiro){
    int i, tabuleiroprint;

    for (i=0, tabuleiroprint = tabuleiro, printf ("\n"); i<9; i++){
        if (tabuleiroprint % 3 == 1)
            printf  (" S  ");
        else if (tabuleiroprint % 3 == 2)
            printf(" O  ");
        else 
            printf  ("    ");
            
        tabuleiroprint = tabuleiroprint /3;
        if (i == 2 || i == 5)
            printf  ("\n----+----+----\n");
        else if (i != 8)   
            printf  ("|");
        }
        return 0;
}

int main (){
    /*
    Tabuleiro:      Armazena a codificação do tabuleiro em base decimal.
    Tabuleiroprint: Variável responsável por fazer operações com o valor do tabuleiro.
    cont:           contador do jogo.
    n:              Variável alternadora de jogadas.
       */
    int linha, coluna, jogada, tabuleiro=0, tabuleiroprint, n, cont, i, start, allsos=0, sos=0, player=0, scorep=0, scorec=0;

    printf  ("*** Bem-vindo ao jogo do SOS! ***\n");
    printf  ("\nDigite 1 se você deseja começar, ou 2 caso contrário: ");
    scanf   ("%d", &start);
    printf  ("configuração inicial:\n");
    printf  ("\n    |    |    \n");
    printf  ("----+----+----\n");
    printf  ("    |    |    \n");
    printf  ("----+----+----\n");
    printf  ("    |    |    \n");
    printf  ("\nO tabuleiro tem %d SOS(s)", sos);
    printf  ("\nPlacar: Usuário %d x %d Computador\n", scorep, scorec);

    if  (start == 1){
        printf  ("\nDigite a sua jogada: ");
        printf  ("\nDigite 1 para S, 2 para O: ");
        scanf   ("%d", &jogada);
        printf  ("Digite a linha: ");
        scanf   ("%d", &linha);
        printf  ("Digite a coluna: ");
        scanf   ("%d", &coluna);
        printf  ("Tabuleiro após sua jogada:\n");
        tabuleiro = jogadaplayer (jogada, linha, coluna, tabuleiro);
    }

    if  (start == 2){
        tabuleiro = jogadabot (tabuleiro);
    }

    maintabuleiro (tabuleiro);
    printf  ("\n\nO tabuleiro tem %d SOS(s)", sos);
    printf  ("\nPlacar: Usuário %d x %d Computador\n", scorep, scorec);
    
    if  (start == 1)
        n = 1;
    else
        n = 0;

    for (cont=1; cont < 9; cont++, n++, player=0, allsos=0){

        if  (n % 2 == 0){
            player = 1;
            printf  ("\nDigite sua jogada:");
            printf  ("\nDigite 1 para S, 2 para O: ");
            scanf   ("%d", &jogada);
            printf  ("Digite a linha: ");
            scanf   ("%d", &linha);
            printf  ("Digite a coluna: ");
            scanf   ("%d", &coluna);
            printf  ("\nTabuleiro após sua jogada:\n");
            tabuleiro = jogadaplayer (jogada, linha, coluna, tabuleiro);
        }
        else
            tabuleiro = jogadabot (tabuleiro);

/*Checagem de SOS:*/
        // Na horizontal
        for (i=0, tabuleiroprint = tabuleiro; i<3; i++){
            if  (tabuleiroprint % 3 == 1 && (tabuleiroprint/3) % 3 == 2 && (tabuleiroprint/9) % 3 ==1)
                allsos++;
            tabuleiroprint = tabuleiroprint / 27;
        }
        // Na vertical
        for (i=0, tabuleiroprint = tabuleiro; i<3; i++){
            if  (tabuleiroprint % 3 == 1 && (tabuleiroprint/27) % 3 == 2 && (tabuleiroprint/729) % 3 ==1)
                allsos++;
            tabuleiroprint = tabuleiroprint / 3;
        }
        // Na diagonal
        tabuleiroprint = tabuleiro;
        if  (tabuleiroprint % 3 == 1 && (tabuleiroprint/81) % 3 == 2 && (tabuleiroprint/6561) % 3 ==1)
            allsos++;

        if  ((tabuleiroprint/9) % 3 == 1 && (tabuleiroprint/81) % 3 == 2 && (tabuleiroprint/729) % 3 ==1)
            allsos++;
        /*
        Allsos = Quantidade total de SOS no tabuleiro na jogada atual.
        sos = Quantidade de SOS da ultima jogada.
        */
        if  ((allsos - sos) != 0){
            if  (player == 1){
                if  (cont == 8)
                    printf  ("Você marcou %d ponto(s).", (allsos - sos));
                else
                    printf  ("Voce marcou %d ponto(s). Jogue novamente.\n", (allsos - sos));
                scorep = scorep + (allsos - sos);
            }
            else{
                if  (cont == 8)
                    printf  ("Marquei %d ponto(s).", (allsos - sos));
                else
                    printf  ("Marquei %d ponto(s). Vou jogar novamente. \n", (allsos - sos));
                scorec = scorec + (allsos - sos);
            }
            n--;
            sos = sos + (allsos - sos);
        }
/*##################################################################################################*/
        maintabuleiro (tabuleiro);
        printf  ("\n\nO tabuleiro tem %d SOS(s)", sos);
        printf  ("\nPlacar: Usuário %d x %d Computador\n", scorep, scorec);
    }
    if  (scorep == scorec)
        printf  ("\nEmpatamos!\n");
    else if (scorep > scorec)
        printf  ("\nVocê ganhou!\n");
    else
        printf  ("\nEu ganhei!\n");
    return 0;
}