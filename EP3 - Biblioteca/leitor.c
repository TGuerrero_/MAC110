/****************************************************************
    Nome: Thiago Guerrero Balera 
    NUSP: 11275297

    Fonte e comentários: 
    Feitos por mim mesmo.
****************************************************************/
#include <stdio.h>
#include <math.h>
#define MAX 1000

//Ordena todas as informações em ordem crescente.
void ordenadados (int D1[MAX], int D2[MAX], int D3[MAX], int D4[MAX], int D5[MAX], int D6[MAX], int n){
    int i, j, aux, troca, check;
    for (i=0, check=1; i < n && check == 1; i++){
        for (j=0, troca=0; j < n-1-i; j++)
            if (D1[j] > D1[j+1]){
                //Primeiro Dado
                aux = D1[j];
                D1[j] = D1[j+1];
                D1[j+1] = aux;
                //Segundo Dado
                aux = D2[j];
                D2[j] = D2[j+1];
                D2[j+1] = aux;
                //Terceiro Dado
                aux = D3[j];
                D3[j] = D3[j+1];
                D3[j+1] = aux;
                //Quarto Dado
                aux = D4[j];
                D4[j] = D4[j+1];
                D4[j+1] = aux;
                //Quinto Dado
                aux = D5[j];
                D5[j] = D5[j+1];
                D5[j+1] = aux;
                //Sexto Dado
                aux = D6[j];
                D6[j] = D6[j+1];
                D6[j+1] = aux;
                troca++;
            }
        if (troca == 0)
            check = 0;
    }
}

//"Limpa" o vetor colocando 0 em todos os seus n elementos.
void clean (int A[MAX], int n){
    int i;
    for (i=0; i < n; i++)
        A[i] = 0;
}

//Checa se o usuário entrou no mesmo dia do relatório ou não e armazena em um vetor definido na "main".
void date (int A[MAX], int B[MAX], int OUT[MAX], int n){
    /*A = inout (Vetor com dados de entrada e saída);
      B = nusp (Vetor com dados dos NUSP's);
      OUT = Vetor de saida;
    */
    int i, j, check;

    for (i= n-1, check=1; i >= 0; i--, check=1){
        for (j=i-1; j >= 0 && check == 1; j--){
            if (A[i] == 1){
                if (B[i] == B[j] && A[j] == 0){
                    OUT[i] = 0;
                    check = 0;
                }
                else
                    OUT[i] = 1;
            }
            else{
                OUT[i] = 0;
                check = 0;
            }
        }
    }
    /* A função analisa todas as informações de saída (denotada pelas linhas que começam com 1)
    e checa se esse mesmo usuário entrou ou não no mesmo dia, se sim, recebe o valor 0, caso
    contrário, recebe 1.*/
}


int main (){
    FILE * log;
    int inout[MAX], nusp[MAX], horas[MAX], min[MAX]; /*Vetores que guardam as informações lidas*/
    int i, n, j, check, start=-1; /*Contadores e auxiliadores*/
    int users, visits, maxperm, distr, histograma[24], perm[MAX], entrance[MAX];/*Dados*/
    float pmedia, pdesvio;
    char logtext[80];
     /*                       DADOS:
    histograma:                 Armazena os dados do histograma.
    perm:                       Armazena os dados de permanência de cada usuário.
    entrance:                   Retorno da função "date", armazena se o usuário entrou em outro dia ou não.
    users e visits:             Número de usuários diferentes e de visitantes, respectivamente.
    pmedia, pdesvio e maxperm:  Dados estatísticos.
    distr:                      Auxilia armazenando a distribuição dos usuários por tempo de uso.
    */

    printf ("\nBem vindo ao Sistema Estatístico de Uso do Salão de Estudos\n");
    printf ("Biblioteca 'Carlos Benjamim de Lyra' - IME-USP\n");
    printf ("\nDigite o nome do arquivo de dados: ");
    scanf ("%s", logtext);
    log = fopen(logtext, "r");
    
    for (n=0; !feof (log); n++){
        if (fscanf (log, "%d %d %d:%d", &inout[n], &nusp[n], &horas[n], &min[n]) != 4){
         n--;
         continue;
        }
    }

//#################################################################################################

    //Pré-operações com os vetores:
    clean (perm, n);
    clean (histograma, 24);
    date (inout, nusp, entrance, n);
    ordenadados (nusp, horas, min, perm, inout, entrance, n);//Ordena em função do nusp.

    //Calculo do tempo de permanência:
    for (i=0, check=1; i < n; i++, check=1){
        for (j=i+1; j <= n && check == 1; j++){
            if (inout[i] == 0 && nusp[i] == nusp[j]){
                perm[i] = ((horas[j]*60)+min[j]) - ((horas[i]*60)+min[i]);
                check = 0;
            /*Fixa-se a posição "i" e faz a comparação com todas as próximas posições procurando
            quando o nusp fixado na posição "i" aparece de novo*/
            }
            else if (inout[i] == 0) /*Caso no qual o usuário entra mas não sai no mesmo dia*/
                perm[i] = (24*60-1) - ((horas[i]*60)+min[i]);
            else if (entrance[i] == 1) /*Caso no qual o usuário entrou em outro dia e saiu no dia do relatório*/
                perm[i] = ((horas[i]*60)+min[i]);
        }
    }

    //Contabiliza a quantidade de usuários diferentes e as visitas:
    for (i=0, users=0, visits=0; i < n; i++){
        if (nusp[i] != nusp[i+1])
            users++;
        if (inout[i] == 0 || entrance[i] == 1)
            visits++;
    }

    //Cálculo da permanência média, maior tempo de permanência e desvio padrão:
    for (i=0, pmedia=0.0; i < n; i++){
        pmedia += perm[i];
    }
    pmedia = (pmedia/visits);

    for (i=0, maxperm=0; i < n; i++){
        if (perm[i] > maxperm)
            maxperm = perm[i];
    }

    for (i=0, pdesvio= 0.0; i < n; i++){
        if (perm[i] != 0)
            pdesvio += pow((perm[i] - pmedia), 2);
    }
    pdesvio = (pdesvio/users);
    pdesvio = sqrt(pdesvio);

    //Montagem do histograma:
    for (i=0; i < n; i++){ //Contagem inicial das pessoas que entraram entre 00 até 01h e as que já estavam desde o dia anterior.
        if ((horas[i] == 0 && inout[i] == 0) || (horas[i] != 0 && entrance[i] == 1))
            histograma[0]++;
    }

    for (i=1; i < 24; i++){ //Manipulação no valor do histograma em função das novas saídas e entradas a cada hora.
        histograma[i] = histograma[i-1];
        for (j=0; j < n; j++){
            if (inout[j] == 1 && horas[j] == i)
                histograma[i]--;
            else if (inout[j] == 0 && horas[j] == i)
                histograma[i]++;
        }
    }

//################################################################################################

    while (start != 0){
    printf ("\n##############################\n");
    printf ("  *Opções de relatórios*");
    printf ("\n##############################\n");
    printf ("\n1 - lista ordenada pelo NUSP dos usuários do salão;\n");
    printf ("2 - lista ordenada pelo tempo total de visita dos usuários do salão;\n");
    printf ("3 - Histograma do uso do salão;\n");
    printf ("4 - Estatísticas do uso do salão;\n");
    printf ("5 - Distribuição do número de usuários por tempo de uso diário em minutos;\n");
    printf ("Se deseja sair e não gerar mais relatórios digite '0'.\n");
    printf ("\nDigite qual relatório você deseja gerar: ");
    scanf  ("%d", &start);

    if (start == 1){
        printf ("\nLISTA DE USUÁRIOS DO SALÃO DE LEITURA\n\n");
        printf ("NUSP       Hora de entrada     Tempo de permanência (minutos)\n");

        for (i=0; i < n; i++){
            if (inout[i] == 0)
                printf ("%8.d       %02d:%02d               %4.d\n", nusp[i], horas[i],min[i], perm[i]);
            else if (entrance[i] == 1)
                printf ("%8.d       00:00               %4.d\n", nusp[i], perm[i]);
        }
    }

    else if (start == 2){
        ordenadados (perm, horas, min, nusp, inout, entrance, n);//Ordena em função da permanência.
        printf ("\nVISITAS ORDENADAS PELO TEMPO DE PERMANÊNCIA\n\n");
        printf ("NUSP       Hora de entrada     Tempo de permanência (minutos)\n");

        for (i=n-1; i >= 0; i--){
            if (inout[i] == 0)
                printf ("%8.d       %02d:%02d               %4.d\n", nusp[i], horas[i],min[i], perm[i]);
            else if (entrance[i] == 1)
                printf ("%8.d       00:00               %4.d\n", nusp[i], perm[i]);
        }
    }

    else if (start == 3){
        printf ("\nHISTOGRAMA DO USO DO SALÃO\n\n");
        for (i=0; i < 24; i++){ //Print do histograma.
            printf (" %02d:00 - %02d:59 : ", i, i);
            for (j=0; j < histograma[i]; j++)
                printf ("*");
            printf ("\n");
        }
    }

    else if (start == 4){
        printf ("\nESTATÍSTICAS DO PERÍODO\n\n");
        printf ("NO PERÍODO TIVEMOS %d USUÁRIOS DIFERENTES E %d VISITAS;\n", users, visits);
        printf ("A MÉDIA DE PERMANÊNCIA NO SALÃO FOI DE %.2f MINUTOS;\n", pmedia);
        printf ("O MAIOR TEMPO DE PERMANÊNCIA FOI %d MINUTOS;\n", maxperm);
        printf ("O DESVIO PADRÃO FOI DE %.2f.\n", pdesvio);
    }

    else if (start == 5){
        printf ("\nDISTRIBUIÇÃO DE USUÁRIOS POR TEMPO DE USO\n\n");
        for (i=0, distr=0; i < 15; i++, distr=0){
            for (j=0; j < n; j++){
                if (perm[j] > i*100 && perm[j] < i*100+100)
                    distr++;
            }
            if (distr == 1)
                printf ("1 Pessoa  ficou   no salão entre %4d e %4d minutos\n", i*100, i*100+99);
            else
                printf ("%d Pessoas ficaram no salão entre %4d e %4d minutos\n", distr, i*100, i*100+99);
        }
    }
    }

    return 0;
} 