/****************************************************************
    Nome: Thiago Guerrero Balera
    NUSP: 11275297

    Fonte e comentários:
    Feitos por mim mesmo.
****************************************************************/
#include <stdio.h>

double modulo (double x){
    if (x < 0)
        return (-x);
    else
        return (x);
}

double seno (double x){
    #define eps 0.00000001
    double seno=0, termo=x;
    int k, i;
    for(k=0, i=0; (modulo(termo)) > eps; k++, i = i + 2){
        if (k%2 == 0)
            seno = seno + termo;
        else
            seno = seno - termo;
        termo = (termo * x * x)/((i+2) * (i+3));
    }
    return (seno);
}

double frac (double x){
    #define c1 831.14159265359
    #define c2 831.71828182846
    double conta;
    conta = (c1 * modulo(seno(x)) + c2);

    return (conta - (int) conta);
}

int main (){

    float seed, porc;
    double x;
    int n, carta, scorep, scoreb, limiar, aux, pwins, ast;
    /*
    porc: Porcentagem de vitórias.
    scorep/scoreb: Somatória da mão do player/bot durante uma rodada de 21.
    pwins: Vezes que o jogador ganhou.
    ast: Variável para printar os asteriscos.
    */


    printf  ("Digite a semente (0 < x < 1): ");
    scanf   ("%f", &seed);
    printf  ("Digite o número de simulações para cada limiar: ");
    scanf   ("%d", &n);
    x = frac (seed);

    for (limiar=12; limiar <= 20; limiar++){
        for (pwins=0, scorep=0, scoreb=0, aux=n; aux > 0; aux--, scorep=0, scoreb=0){
            while (scorep < limiar){
                carta = (13 * x + 1);
                x = frac (x);
            //Considerando as cartas 11, 12 e 13 equivalentes às cartas K, J e Q, temos:
                if  (carta <= 10)
                    scorep = scorep + carta;
                else 
                    scorep = scorep + 10;
            }

            while (scoreb < scorep && scorep < 21){
                carta = (13 * x + 1);
                x = frac (x);
                if  (carta <= 10)
                    scoreb = scoreb + carta;
                else if (carta > 10)
                    scoreb = scoreb + 10;
            }

            if  ( (scorep > scoreb && scorep < 21) || scorep == 21 || scoreb > 21)
                pwins++;
        }
        porc = (float)(pwins*100)/(float)n;  //Calcula a porcentagem de vitórias do jogador.
        printf  ("  %d  (%.1f%%) : ", limiar, porc);
        for (ast = porc; ast > 0; ast--)
            printf  ("*");
        printf  ("\n");
    }
        return 0;
}